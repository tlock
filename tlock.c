/*
 * Copyright (c) 2017, S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <errno.h>
#include <pwd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#include "macros.h"

static void jack(int s)
{
        UNUSED(s);
}

static int get_username(const char **out_username)
{
        struct passwd *passwd;

        errno = 0;

        if (!(passwd = getpwuid(getuid()))) {
                PERROR_MESSAGE("getpwuid");

                return -1;
        }

        *out_username = passwd->pw_name;

        return 0;
}

static void redraw_screen(int failed)
{
        struct winsize ws;
        struct timespec req = { .tv_sec = 0, .tv_nsec = 500000000 };
        int midcol = 0;

        ioctl(fileno(stdin), TIOCGWINSZ, &ws);
        printf("\033[2J");

        if (failed) {
                printf("\033[48;2;%d;%d;%dm", 0xcc, 0x33, 0x33);
        } else {
                printf("\033[48;2;%d;%d;%dm", 0x88, 0x88, 0x88);
        }

        for (int row = 0; row <= ws.ws_row; ++row) {
                printf("\033[%d;0f", row);

                for (int j = 0; j < ws.ws_col; ++j) {
                        printf(" ");
                }
        }

        printf("\033[0m");
        fflush(stdout);
        printf("\033[%d;%df", ws.ws_row / 2 - 1, 0);
        nanosleep(&req, 0);
        req.tv_nsec = 500000000 / ws.ws_col;
        midcol = ws.ws_col / 2;

        for (int j = 0; j <= ws.ws_col / 2 + 1; ++j) {
                nanosleep(&req, 0);

                /* left */
                if (midcol - j >= 0) {
                        printf("\033[%d;%df", ws.ws_row / 2 - 1, midcol - j);
                        printf("─");
                        printf("\033[%d;%df", ws.ws_row / 2, midcol - j);
                        printf(" ");
                        printf("\033[%d;%df", ws.ws_row / 2 + 1, midcol - j);
                        printf("─");
                }

                /* right */
                if (midcol + j <= ws.ws_col) {
                        printf("\033[%d;%df", ws.ws_row / 2 - 1, midcol + j);
                        printf("─");
                        printf("\033[%d;%df", ws.ws_row / 2, midcol + j);
                        printf(" ");
                        printf("\033[%d;%df", ws.ws_row / 2 + 1, midcol + j);
                        printf("─");
                }

                fflush(stdout);
        }

        fflush(stdout);
        printf("\033[%d;%df", ws.ws_row / 2, 5);
        fflush(stdout);
}

static void get_correct_password(const char *username)
{
        pid_t pid = 0;
        int status = 0;
        int failed_yet = 0;

        while (1) {
                redraw_screen(failed_yet);
                pid = fork();

                if (!pid) {
                        /* We are child */
                        execl("/bin/su", "su", "-", username, "-c", "true",
                              (char *) 0);
                        _exit(-1);
                }

                if (waitpid(pid, &status, 0) < 0) {
                        /* Waitpid failed. Too bad, user */
                        continue;
                }

                if (WIFEXITED(status) &&
                    WEXITSTATUS(status) == 0) {
                        break;
                }

                failed_yet = 1;
        }
}

static int ignore_signals(void)
{
        struct sigaction sa = { .sa_handler = jack, .sa_flags = SA_RESTART };

        sigfillset(&sa.sa_mask);

        if (sigaction(SIGUSR1, &sa, 0) < 0) {
                PERROR_MESSAGE("sigaction");

                return -1;
        }

        if (sigaction(SIGUSR2, &sa, 0) < 0) {
                PERROR_MESSAGE("sigaction");

                return -1;
        }

        if (sigaction(SIGINT, &sa, 0) < 0) {
                PERROR_MESSAGE("sigaction");

                return -1;
        }

        if (sigaction(SIGTSTP, &sa, 0) < 0) {
                PERROR_MESSAGE("sigaction");

                return -1;
        }

        return 0;
}

int main(void)
{
        const char *username = 0;

        if (ignore_signals() < 0) {
                goto done;
        }

        printf("\033[3J");
        printf("\033[?25l");
        fflush(stdout);

        if (get_username(&username) < 0) {
                goto done;
        }

        get_correct_password(username);
        printf("\033[0m");
        printf("\033[0;0f");
        printf("\033[2J");
done:
        printf("\033[?25h");

        return 0;
}
