.SUFFIXES:
.SUFFIXES: .o .c

CFLAGS ?=
LDFLAGS ?=

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
SHAREDIR ?= $(PREFIX)/share
MANDIR ?= $(PREFIX)/share/man

CFLAGS += -std=c99 -D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=1

# Debug
CFLAGS += -pedantic -Wall -Wextra -Werror

PROG = tlock

default: $(PROG)

$(PROG): tlock.o
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c macros.h
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: clean
clean:
	find -name '*.o' -delete
	rm -f $(PROG)

.PHONY: install
install: $(PROG)
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f $(PROG) $(DESTDIR)$(BINDIR)/
	mkdir -p $(DESTDIR)$(MANDIR)
	cp -f $(PROG).1 $(DESTDIR)$(MANDIR)/

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f $(PROG)
	cd $(DESTDIR)$(MANDIR) && rm -f $(PROG).1
